package steps;

import apis.BaseApiTests;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class BaseApiScenariosSteps {

    @Given("^a Twitter user posts a tweet of \"([^\"]*)\"$")
    public void i_post_a_tweet_of(String message) {
        BaseApiTests.postTweet(message);
    }

    @When("^they retrieve the results of \"([^\"]*)\"$")
    public void i_retrieve_the_results_of(String apiResource) {
        BaseApiTests.getRequest(apiResource);
    }

    @Then("^the most recent tweet in the Home Timeline is \"([^\"]*)\"$")
    public void theMostRecentTweetInTheHomeTimelineIs(String value) {
        BaseApiTests.assertKeyValue("[0].text", value);
    }
}