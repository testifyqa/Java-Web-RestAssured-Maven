@Api
Feature: Test Twitter Tweets

  Scenario: 01. Get recent tweets from our Home Timeline
    Given a Twitter user posts a tweet of "Hello World! This is a test tweet!"
    When they retrieve the results of "/home_timeline.json"
    Then the most recent tweet in the Home Timeline is "Hello World! This is a test tweet!"